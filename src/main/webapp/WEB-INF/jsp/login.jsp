<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="static.jsp"></jsp:include>
    <title>Login page</title>
    <script src="<c:url value="../../../resources/static/js/login.js" />"></script>
    <style>
        #container {
            display: grid;
            grid-template-columns: auto 600px;
        }

        #left {
            background-color: #2980b9;
        }

        #right {
            display: grid;
            grid-template-rows: 30% 2px auto;
        }

        #black-line {
            width: 80%;
            height: 2px;
            background-color: grey;
            margin-left: auto;
            margin-right: auto;
        }

        h2#login-title {
            text-transform: uppercase;
            text-align: center;
            align-self: center;
        }

        input {
            display: block;
        }

        #login-container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        #login-container div {
            display: flex;
            justify-content: space-evenly;
            align-items: center;
            flex-direction: column;
            height: 250px;
            width: 60%;
            position: relative;
        }

        #login-container div.active:after {
            display: block;
        }

        #login-container div:after {
            display: none;
            content: "Username or password is incorrect";
            position: absolute;
            top: 100%;
            left: 50%;
            transform: translateX(-50%);
            width: max-content;
            color: red;
        }

        #login-container-title,
        .login-text-box {
            width: 100%;
            height: 30px;
            border-radius: 5px;
        }

        #login-container button {
            background-color: #0984e3;
            border: none;
            height: 30px;
            width: 80px;
            border-radius: 5px;
            font-weight: 700;
            cursor: pointer;
        }
    </style>
</head>

<body>
<div id="container">
    <div id="left"></div>
    <div id="right">
        <h2 id="login-title">Quản Lý Khuyến Mãi</h2>
        <div id="black-line"></div>
        <div id="login-container">
            <div>
                <h2 id="login-container-title">Đăng nhập</h2>
                <input type="text" class="login-text-box" id="login-username" placeholder="Username">
                <input type="password" class="login-text-box" id="login-password" placeholder="Password">
                <button type="button" id="login-btn">Đăng nhập</button>
            </div>
        </div>
    </div>
</div>
</body>

</html>