<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="static.jsp"></jsp:include>
    <title>List user</title>
    <style>
        #container {
            display: flex;
            flex-direction: column;
        }

        #container-inner {
            width: 100%;
            height: 100%;
            display: grid;
            grid-template-columns: 300px auto;
            background-color: #dcdde1;
        }

        #add-customer-container {
            padding: 20px;
        }

        #add-customer-container-inner {
            width: 100%;
            height: 100%;
            display: grid;
            grid-template-rows: 50px auto;
        }

        #top {
            display: grid;
            grid-template-columns: 45% 45% auto;
        }

        #actions-container {
            position: relative;
            text-align: right;
        }

        #action-btn {
            cursor: pointer;
        }

        #list-types {
            display: flex;
            flex-direction: row;
            justify-content: space-around;
        }

        .action-item {
            width: 100%;
            height: 30px;
            line-height: 30px;
            cursor: pointer;
        }

        .action-item:nth-child(odd) {
            width: 100%;
            height: 30px;
            line-height: 30px;
            border-bottom: 1px solid;
        }

        #actions-container #action-items {
            display: none;
            position: absolute;
            right: 0%;
            border-radius: 5px;
            border: 1px solid;
            text-align: center;
        }

        #actions-container #action-items.active {
            display: block;
        }

        #bottom {
            padding: 20px 50px 20px 50px;
            background-color: #f5f6fa;
        }

        #list-user-table,
        #list-user-boxes {
            display: none;
        }

        #list-user-table.active {
            display: table;
            width: 100%;
            text-align: center;
        }

        #list-user-table tr {
            height: 30px;
        }

        #list-user-table-header {
            background-color: #dcdde1;
        }

        #list-user-boxes.active {
            display: block;
            display: flex;
            flex-wrap: wrap;
            flex-direction: row;
            justify-content: space-between;
            gap: 10px;
        }

        .user-box {
            background-color: red;
            display: grid;
            grid-template-columns: 60px auto;
            height: 60px;
        }

        .user-box img {
            width: 60px;
            height: 60px;
        }

        nav .active {
            color: #fbc531;
        }

        nav {
            background-color: #34495e;
        }

        nav#main-nav {
            color: #f5f6fa;
            background-color: #2f3640;
            height: 60px;
            display: grid;
            grid-template-columns: auto 100px 100px;
            align-items: center;
            justify-items: center;
        }

        span#logout-btn {
            cursor: pointer;
        }

        nav ul {
            margin-top: 50px;
        }

        nav ul .nav-item {
            height: 30px;
            text-align: center;
            font-size: 25px;
            background-color: #487eb0;
            cursor: pointer;
        }

        #search-coupon-container {
            display: flex;
            width: 100%;
            justify-content: center;
            align-items: baseline;
            margin: 10px 0;
        }

        #search-coupon-text-box {
            padding-left: 5px;
            height: 30px;
            border-radius: 5px;
        }

        #search-coupon-used {
            display: none;
            height: 30px;
            line-height: 30px;
            position: relative;
        }

        #search-coupon-used.active:after {
            content: "";
            width: 100%;
            height: 100%;
            background-color: red;
            position: absolute;
            left: 0;
            z-index: -1;
            opacity: 0.6;
            border-radius: 5px;
        }

        #search-coupon-accept-btn {
            display: none;
        }

        #search-coupon-accept-btn.active {
            background-color: #0984e3;
            border: none;
            height: 30px;
            width: 80px;
            border-radius: 5px;
            font-weight: 700;
            cursor: pointer;
        }
    </style>
</head>

<body>
<div id="container">
    <nav id="main-nav">
        <div></div>
        <span id="full-name">${user.firstName}</span>
        <span id="logout-btn">Log Out</span>
    </nav>
    <div id="search-coupon-container">
        <input type="text" id="search-coupon-text-box" placeholder="Nhập coupon">
        <p id="search-coupon-used">Khách hàng quẹt thẻ vào lúc: <span id="search-coupon-used-time">22:22:22</span>
        </p>
        <button type="button" id="search-coupon-accept-btn">Xác nhận</button>
    </div>
    <div id="container-inner">
        <nav id=nav-bar>
            <ul>
                <li class="nav-item active">Quản lý khách hàng</li>
                <li class="nav-item">Quản lý cửa hàng</li>
            </ul>
        </nav>
        <div id="add-customer-container">
            <div id="add-customer-container-inner">
                <div id="top">
                    <div id="search-container">
                        <input type="text" placeholder="Nội dung tìm kiếm">
                        <button type="button">Tìm</button>
                    </div>
                    <div id="actions-container">
                        <p id="action-btn">Thực hiện</p>
                        <div id="action-items">
                            <p class="action-item">Thêm khách hàng</p>
                            <p class="action-item">Xóa khách hàng</p>
                        </div>
                    </div>
                    <div id="list-types">
                        <span>xxx</span>
                        <span>yyy</span>
                    </div>
                </div>
                <div id="bottom">
                    <table id="list-user-table" class="active" cellspacing="0">
                        <tr id="list-user-table-header">
                            <th><input type="checkbox" id="list-user-select-all-chbx"
                                       class="list-user-select-user-chbx"></th>
                            <th>Họ</th>
                            <th>Tên</th>
                            <th>Phone</th>
                            <th>Năm sinh</th>
                            <th>KM</th>
                            <th>Giờ</th>
                            <th>Địa chỉ</th>
                            <th>Khác</th>
                        </tr>
                        <tr>
                            <td><input type="checkbox" id="list-user-select-all-chbx-1"
                                       class="list-user-select-user-chbx"></td>
                            <td>Nguyễn Văn</td>
                            <td>A</td>
                            <td>123456789</td>
                            <td>14/05/1999</td>
                            <td><input type="radio" disabled=true></td>
                            <td>09:09</td>
                            <td>abc</td>
                            <td>dè</td>
                        </tr>
                    </table>
                    <div id="list-user-boxes">
                        <div id="user-box1" class="user-box">
                            <img src="" alt="abc">
                            <d id="user-box-information">
                                <p class="user-box-name">Họ Tên: <span>Nguyen Van A</span></p>
                                <p class="user-box-address">Địa chỉ: <span>abc</span></p>
                                <p class="user-box-phone">Phone: <span>12345</span></p>
                            </d>
                        </div>
                        <div id="user-box2" class="user-box">
                            <img src="" alt="abc">
                            <d id="user-box-information">
                                <p class="user-box-name">Họ Tên: <span>Nguyen Van A</span></p>
                                <p class="user-box-address">Địa chỉ: <span>abc</span></p>
                                <p class="user-box-phone">Phone: <span>12345</span></p>
                            </d>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>