<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add customer</title>
    <style>
        html,
        body,
        #container {
            width: 100%;
            height: 100vh;
        }

        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        #container {
            display: flex;
            flex-direction: column;
        }

        #container-inner {
            width: 100%;
            height: 100%;
            display: grid;
            grid-template-columns: 300px auto;
            background-color: #dcdde1;
        }

        #add-customer-container {
            padding: 20px;
        }

        #add-customer-container-inner {
            width: 100%;
            height: 100%;
            display: grid;
            grid-template-rows: 50px auto;
        }

        #top {
            display: grid;
            grid-template-columns: 45% 45% auto;
        }

        #actions-container {
            position: relative;
            text-align: right;
        }

        #action-btn {
            cursor: pointer;
        }

        #list-types {
            display: flex;
            flex-direction: row;
            justify-content: space-around;
        }

        .action-item {
            width: 100%;
            height: 30px;
            line-height: 30px;
            cursor: pointer;
        }

        .action-item:nth-child(odd) {
            width: 100%;
            height: 30px;
            line-height: 30px;
            border-bottom: 1px solid;
        }

        #actions-container #action-items {
            display: none;
            position: absolute;
            right: 0%;
            border-radius: 5px;
            border: 1px solid;
            text-align: center;
        }

        #actions-container #action-items.active {
            display: block;
        }

        #bottom {
            padding: 20px 50px 20px 50px;
            background-color: #f5f6fa;
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
        }

        nav .active {
            color: #fbc531;
        }

        nav {
            background-color: #34495e;
        }

        nav#main-nav {
            background-color: #2f3640;
            height: 60px;
        }

        nav ul {
            margin-top: 50px;
        }

        nav ul .nav-item {
            height: 30px;
            text-align: center;
            font-size: 25px;
            background-color: #487eb0;
            cursor: pointer;
        }
    </style>
</head>

<body>
<div id="container">
    <nav id="main-nav">12</nav>
    <div id="container-inner">
        <nav id=nav-bar>
            <ul>
                <li class="nav-item active">Quản lý khách hàng</li>
                <li class="nav-item">Quản lý cửa hàng</li>
            </ul>
        </nav>
        <div id="add-customer-container">
            <div id="add-customer-container-inner">
                <div id="top">
                    <div id="search-container">
                        <input type="text" placeholder="Nội dung tìm kiếm">
                        <button type="button">Tìm</button>
                    </div>
                    <div id="actions-container">
                        <p id="action-btn">Thực hiện</p>
                        <div id="action-items">
                            <p class="action-item">Thêm khách hàng</p>
                            <p class="action-item">Xóa khách hàng</p>
                        </div>
                    </div>
                    <div id="list-types">
                        <span>xxx</span>
                        <span>yyy</span>
                    </div>
                </div>
                <div id="bottom">
                    <div id="customer-information">
                        <div id="image-preview"></div>
                        <div id="customer-inputs">
                            <div id="input-one">
                                <input type="text" placeholder="Tên lót">
                                <input style="justify-self: flex-end;" type="text" placeholder="Tên">
                                <input type="text" placeholder="Điện thoại">
                                <input style="justify-self: flex-end;" type="text" placeholder="Email">
                            </div>
                            <div id="input-two">
                                <input type="text" placeholder="Địa chỉ">
                                <input type="file" placeholder="Chọn hình">
                                <input type="text" placeholder="Thông tin khác">
                            </div>
                            <button id="add-customer-btn">Thêm</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>