package com.example.demo.service;

import com.example.demo.model.UserCoupon;

import java.util.List;

public interface UserCouponService {
    public List<UserCoupon> findAll();

    public UserCoupon findById(Long id);

    public Integer isUsed(Long userId, Long couponId);
}
