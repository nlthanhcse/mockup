package com.example.demo.service.impl;

import com.example.demo.model.UserCoupon;
import com.example.demo.repository.UserCouponRepository;
import com.example.demo.service.UserCouponService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserCouponServiceImpl implements UserCouponService {
    @Autowired
    private UserCouponRepository userCouponRepository;

    @Override
    public List<UserCoupon> findAll() {
        return userCouponRepository.findAll();
    }

    @Override
    public UserCoupon findById(Long id) {
        return userCouponRepository.findById(id).orElse(null);
    }

    @Override
    public Integer isUsed(Long userId, Long couponId) {
        return userCouponRepository.isUsed(userId, couponId);
    }
}
