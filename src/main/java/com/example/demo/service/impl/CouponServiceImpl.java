package com.example.demo.service.impl;

import com.example.demo.model.Coupon;
import com.example.demo.repository.CouponRepository;
import com.example.demo.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CouponServiceImpl implements CouponService {
    @Autowired
    private CouponRepository couponRepository;
    @Override
    public List<Coupon> findAll() {
        return couponRepository.findAll();
    }

    @Override
    public Coupon findById(Long id) {
        return couponRepository.findById(id).orElse(null);
    }
}
