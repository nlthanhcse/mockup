package com.example.demo.service.impl;

import com.example.demo.model.Shop;
import com.example.demo.repository.ShopRepository;
import com.example.demo.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ShopServiceImpl implements ShopService {
    @Autowired
    private ShopRepository shopRepository;

    @Override
    public List<Shop> findAll() {
        return shopRepository.findAll();
    }

    @Override
    public Shop findById(String id) {
        return shopRepository.findById(id).orElse(null);
    }
}
