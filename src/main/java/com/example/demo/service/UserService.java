package com.example.demo.service;

import com.example.demo.model.User;

import java.util.List;

public interface UserService {

    public List<User> findAll();

    public User findById(Long id);

    /**
     * This function will find the user with responding username and password
     *
     * @param username the username
     * @param password the password
     * @return {@link User} a user if user name and password match
     *          or null
     */
    public User getUserByUsernameAndPasswordWithRoleAdmin(String username, String password);
}
