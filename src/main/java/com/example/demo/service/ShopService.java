package com.example.demo.service;

import com.example.demo.model.Shop;

import java.util.List;

public interface ShopService {
    public List<Shop> findAll();

    public Shop findById(String id);
}
