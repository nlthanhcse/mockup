package com.example.demo.service;

import com.example.demo.model.Coupon;

import java.util.List;

public interface CouponService {
    public List<Coupon> findAll();

    public Coupon findById(Long id);
}
