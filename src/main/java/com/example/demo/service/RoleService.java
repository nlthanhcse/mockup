package com.example.demo.service;

import com.example.demo.model.Role;

import java.util.List;

public interface RoleService {
    public List<Role> findAll();

    public Role findById(Long id);
}
