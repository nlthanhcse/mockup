package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;

@Controller
@SessionAttributes("user")
public class LoginController {
    @Autowired
    private UserService userService;

    @GetMapping({"/", "/login"})
    public String login() {
        return "login";
    }

    @PostMapping("/login")
    public ResponseEntity<Boolean> executeLogin(@RequestBody User user, ModelMap modelMap) {
        User loginUser = userService.getUserByUsernameAndPasswordWithRoleAdmin(user.getUsername(), user.getPassword());
        if (loginUser == null) {
            return ResponseEntity.ok(Boolean.FALSE);
        }
        modelMap.addAttribute("user", loginUser);
        return ResponseEntity.ok(Boolean.TRUE);
    }

    @GetMapping("/logout")
    public String logout(SessionStatus sessionStatus) {
        sessionStatus.setComplete();
        return "redirect:./";
    }
}
