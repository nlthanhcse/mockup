package com.example.demo.repository;

import com.example.demo.model.UserCoupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCouponRepository extends JpaRepository<UserCoupon, Long> {
    @Query(value = "SELECT DATE(CAST(us.last_usage as DATETIME)) = CURDATE() FROM user_coupon uc WHERE uc.user_id = :userId AND uc.coupon_id = :couponId", nativeQuery = true)
    public Integer isUsed(@Param("userId") Long userId, @Param("couponId") Long couponId);
}
