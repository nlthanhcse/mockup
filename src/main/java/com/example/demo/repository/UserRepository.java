package com.example.demo.repository;

import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value = "SELECT u.* FROM user u, role r WHERE u.username = :username AND u.password = :password" +
            " AND u.role_id = r.id" +
            " AND r.name = 'ROLE_ADMIN'", nativeQuery = true)
    User getUserByUsernameAndPasswordWithRoleAdmin(@Param("username") String username, @Param("password") String password);
}
