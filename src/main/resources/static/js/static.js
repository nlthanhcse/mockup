window.addEventListener("DOMContentLoaded", () => {
    const logoutBtn = document.querySelector("#logout-btn");

    // Add eventListener area
    if (logoutBtn !== undefined) {
        logoutBtn.addEventListener("click", () => {
            logout();
        });
    }
});
// Global element


// Global variables
const ACTIVE_CLASS = "active";

// Global functions
/**
 * Add classes for an element
 *
 * @param element
 * @param classes
 */
function addClass(element, ...classes) {
    for (let i = 0; i < classes.length; i++) {
        element.classList.add(classes[i]);
    }
}

/**
 * Call an endpoint to logout
 */
function logout() {
    location.href = "./logout";
}