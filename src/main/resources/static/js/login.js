window.addEventListener("DOMContentLoaded", () => {
    const loginBtn = document.querySelector("#login-btn");
    const userNameEle = document.querySelector("#login-username");
    const passwordEle = document.querySelector("#login-password");
    const loginFailedMsqEle = document.querySelector("#login-container div");

    // Login btn clicked event
    loginBtn.addEventListener("click", () => {
        login(123);
    });

    /**
     * This function will use AJAX to call login endpoint to check username and password
     */
    function login() {
        let userName = userNameEle.value;
        let password = passwordEle.value;

        let user = {
            username: userName,
            password: password
        };

        $.ajax({
            url: "./login",
            type: "POST",
            dataType: "json",
            data: JSON.stringify(user),
            contentType: "application/json;charset=UTF8",
            async: false,
            success: function (data) {
                if (data) {
                    // go to management screen
                    location.href = "./list-customer";
                } else {
                    addClass(loginFailedMsqEle, ACTIVE_CLASS);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
})