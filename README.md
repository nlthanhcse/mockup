* # Database Design (No relationship)
    * Role: id: Long, name: String
    * Shop: id: String, address: String
    * User: id: Long, username: String, password: String, first_name: String, last_nam: String, phone_number: String, email: String, address: String, image_url: String, other: String, role_id: Long, shop_id
    * Coupon: id: Long, code: String
    * Coupon_User: id: Long, user_id: Long, coupon_id: Long, last_usage: String

* # Functional requirements
  * 1. Admin login
  * 2. Admin management customers of each shop_id
  	* 2.1. Add new customer
  	* 2.2. Update an existing customer
  	* 2.3. Remove an existing customer
  	* 2.4. Generate coupon code
  	* 2.5. Export list customer to Excel file
  
* # Project information
  * 1. Port: 9999
  * 2. Database name: mockup
